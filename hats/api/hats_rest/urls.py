from django.urls import path 
from .views import hats, hat_details

urlpatterns = [
    path("hats/", hats, name="hats"),
    path("hats/<int:id>", hat_details, name="hat_details")
]
