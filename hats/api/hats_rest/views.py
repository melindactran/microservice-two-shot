from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "color",
        "style",
        "picture",
        "id"
    ]


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href"
    ]
    
@require_http_methods(["GET", "POST"])
def hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )

    else:
        content = json.loads(request.body)
    try:
        location_href = content.get("location") 
        location = LocationVO.objects.get(id=location_href)
        content["location"] = location
    except LocationVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location ID"},
            status=400,
        )

    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatEncoder,
        safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def hat_details(request, id):
    if request.method == "GET":
        try: 
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat, 
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
    elif request.method == "DELETE":
        try: 
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse(
                {"message": "Hat deleted successfully"},
                status=204
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            hat = Hat.objects.get(id=id)
            content = json.loads(request.body)
            Hat.objects.filter(id=id).update(**content)
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    else:
        response = JsonResponse({"message": "Method not allowed"})
        response.status_code = 405
        return response



