# Wardrobify

Team:
* Person 1 - Which microservice? Marvin Xia - Hats
* Person 2 - Which microservice? Melinda Tran - Shoes

## Design

## Shoes microservice

Shoes API models (Shoes, BinVo) are integrated with the wardrobe API in order to achieve functionalities such as viewing a list of shoes, creating a new shoe and deleting a shoe because that information needs to be gathered from the appropriate sections.

## Hats microservice

The Hat microservice starts out with the use of two specific models in the backend: the Hat and LocationVO model. For the Hat Model a user is able to specify particular information regarding the hat they are adding to their app such as color, style, fabric, name and picture. In addition to these parameters the Hat model also shares a foreign key relationship with the LocationsVO. This relationship specifically indicates for a user when they add a new hat to select a pre-created location/closet to place their hat in. 

In order for anything to be created with these models a views function must be used. In the views there are two encoders and view functions that are utilized. The encoders the HatEncoder and the LocationVoEncoder and the views Hats and hat_details. In the views for Hats a user is able to create a new hat object as well get a display of all created hats. In the hat_details the user is able to delete, edit and see an individual hat of their choice. 

Moving to the front end, the user is able to utilize an intuitively designed web application to move around and select to see their listed shoes or hats. In the Hats section a user is able to create a hat or view the hat(s) that have been just created.  