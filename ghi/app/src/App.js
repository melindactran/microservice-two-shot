import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './hats/HatsList';
import HatsForm from './hats/HatsForm';
import ShoesList from './shoes/ShoesList';
import ShoesForm from './shoes/ShoesForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="hats">
            <Route index element={<HatsList hats={HatsList} />} />
            <Route path="add-hat" element={<HatsForm />} />
          </Route>

          <Route path="shoes">
            <Route index element={<ShoesList shoes={ShoesList} />} />
            <Route path="add-shoes" element={<ShoesForm /> } />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
