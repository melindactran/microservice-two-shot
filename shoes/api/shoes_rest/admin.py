from django.contrib import admin
from .models import Shoes, BinVo

# Register your models here.


@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    pass


@admin.register(BinVo)
class BinVoAdmin(admin.ModelAdmin):
    pass
