from django.db import models

# Create your models here.


class BinVo(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveIntegerField()
    bin_size = models.PositiveIntegerField()

    def __str__(self):
        return self.closet_name


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=2000, null=True)

    bin = models.ForeignKey(
        BinVo,
        related_name='shoes',
        on_delete=models.PROTECT)

    def __str__(self):
        return self.model_name

    class Meta:
        ordering = ("manufacturer", "model_name")
